 //This method (ExistsInEnum) checks a string input value to see if it is contained in the given enum
        //This method is case INsensitive
        //There is a strong possibility this method could be made dynamic but, could be risky and slightly more expensive for rare use
        public static bool ExistsInEnum<T>(string input)
        {
            input = input.ToLower();

            foreach (var current in Enum.GetValues(typeof(T)))
            {
                //Does input exist in enum?
                if (current.ToString().ToLower() == input)
                {
                    return true;
                }
            }

            //If we have made it out of the loop, it has failed validation
            return false;
        }